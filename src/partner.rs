// This file is part of a user study. Please be aware of the following information:
// The source code is not intended for use in production and should not be reproduced
// or distributed without prior written permission. The authors of this file are in
// no way liable for any damages that occur as a result of its use.

use borsh::{BorshDeserialize, BorshSerialize};
use solana_program::{
    account_info::{next_account_info, AccountInfo},
    entrypoint::ProgramResult,
    instruction::{AccountMeta, Instruction},
    program::invoke_signed,
    pubkey::Pubkey,
};

/// Users can apply coupons, e.g. to get airdrop tokens via partner programs.
/// A coupon is only valid until a specific timestamp is reached.
#[repr(C)]
#[derive(Clone, Copy, Debug, Default, PartialEq, BorshSerialize, BorshDeserialize)]
pub struct Coupon {
    pub amount: u64,
    pub partner_prog: Pubkey,
    pub valid_through: i64,
}

/// The marketplace processes the PartnerInstruction together with the PDA seed to validate authenticity of a coupon.
#[derive(Debug, BorshDeserialize, BorshSerialize)]
pub enum PartnerInstruction {
    RefreshCredit { seed: String },
    ApplyCoupon { seed: String },
}

/// Partner know roughly the same Instructions, but those programs do not require seeds.
#[derive(Debug, BorshDeserialize, BorshSerialize)]
pub enum ExtPartnerInstructions {
    RefreshCredit,
    ApplyCoupon,
}

/// This function applies the coupon and adds its tokens to the TokenVault.
pub fn apply_coupon(program_id: &Pubkey, accounts: &[AccountInfo], seed: String) -> ProgramResult {
    let account_info_iter = &mut accounts.iter();

    let coupon_info = next_account_info(account_info_iter)?;
    let partner_prog = next_account_info(account_info_iter)?;
    let vault_info = next_account_info(account_info_iter)?;

    let (c, cb) = Pubkey::find_program_address(&[program_id.as_ref(), seed.as_bytes()], program_id);

    assert_eq!(*coupon_info.key, c);
    assert_eq!(vault_info.owner, program_id);
    assert_eq!(coupon_info.owner, program_id);
    assert!(partner_prog.executable);
    let c = Coupon::try_from_slice(&coupon_info.data.borrow_mut())?;
    assert_eq!(c.partner_prog, *partner_prog.key);

    let instruction = Instruction::new_with_borsh(*partner_prog.key, &ExtPartnerInstructions::ApplyCoupon, vec![AccountMeta::new(*coupon_info.key, true), AccountMeta::new(*vault_info.key, false)]);
    invoke_signed(&instruction, &[coupon_info.clone(), vault_info.clone()], &[&[program_id.as_ref(), seed.as_bytes(), &[cb]]])
}

/// This function is used to ask a partner program to initialize an empty coupon or to update the current value of the coupon.
pub fn refresh_credit(program_id: &Pubkey, accounts: &[AccountInfo], seed: String) -> ProgramResult {
    let account_info_iter = &mut accounts.iter();

    let coupon_info = next_account_info(account_info_iter)?;
    let partner_prog = next_account_info(account_info_iter)?;

    let (coupon_key, coupon_bump) =
        Pubkey::find_program_address(&[program_id.as_ref(), seed.as_bytes()], program_id);

    assert_eq!(*coupon_info.key, coupon_key);
    assert_eq!(coupon_info.owner, program_id);
    assert!(partner_prog.executable);
    let instruction = Instruction::new_with_borsh(
        *partner_prog.key,
        &ExtPartnerInstructions::RefreshCredit,
        vec![AccountMeta::new(coupon_key, true)]
    );
    invoke_signed(
        &instruction,
        &[coupon_info.clone()],
        &[&[program_id.as_ref(), seed.as_bytes(), &[coupon_bump]]]
    )
}
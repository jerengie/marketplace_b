// This file is part of a user study. Please be aware of the following information:
// The source code is not intended for use in production and should not be reproduced
// or distributed without prior written permission. The authors of this file are in
// no way liable for any damages that occur as a result of its use.

use crate::entry::User;
use borsh::{BorshDeserialize, BorshSerialize};
use solana_program::{
    account_info::{next_account_info, next_account_infos, AccountInfo},
    entrypoint::ProgramResult,
    program::{invoke, invoke_signed},
    pubkey::Pubkey,
    rent::Rent,
    system_instruction,
};

const PRICE: u64 = 1000000; // 0.001 SOL

/// A TokenVault stores the amount of tokens owned by a user.
#[repr(C)]
#[derive(Clone, Copy, Debug, Default, PartialEq, BorshSerialize, BorshDeserialize)]
pub struct TokenVault {
    pub user: Pubkey,
    pub amount: u64,
}

/// The marketplace supports some TokenVault functionality:
/// - initializing an empty vault
/// - buying tokens with lamports
/// - sharing (distributing) tokens with a number of vaults
/// - selling tokens for lamports
/// - peer to peer transfer of tokens
#[derive(Debug, BorshDeserialize, BorshSerialize)]
pub enum TokenInstruction {
    Initialize,
    Buy { amount: u64 },
    Distribute { amount: u64 },
    Sell { amount: u64 },
    Transfer { amount: u64 },
}

/// This function initializes a new vault for a given user.
pub fn initialize(program_id: &Pubkey, accounts: &[AccountInfo]) -> ProgramResult {
    let account_info_iter = &mut accounts.iter();

    let vault_info = next_account_info(account_info_iter)?;
    let user_info = next_account_info(account_info_iter)?;
    let authority_info = next_account_info(account_info_iter)?;

    let (vault_key, vault_bump) =
        Pubkey::find_program_address(&[authority_info.key.as_ref(), b"+vault"], program_id);

    assert!(authority_info.is_signer);
    assert_eq!(*vault_info.key, vault_key);

    invoke_signed(
        &system_instruction::create_account(
            authority_info.key,
            vault_info.key,
            Rent::default().minimum_balance(42),
            42,
            program_id,
        ),
        &[authority_info.clone(), vault_info.clone()],
        &[&[authority_info.key.as_ref(), b"+vault", &[vault_bump]]],
    )?;

    let mut vault = TokenVault::try_from_slice(&vault_info.data.borrow())?;
    vault.user = *user_info.key;
    vault.amount = 0;
    vault.serialize(&mut *vault_info.data.borrow_mut())?;

    Ok(())
}

/// Using this function, users can buy a given amount of tokens.
pub fn buy(program_id: &Pubkey, accounts: &[AccountInfo], amount: u64) -> ProgramResult {
    let account_info_iter = &mut accounts.iter();

    let vault_info = next_account_info(account_info_iter)?;
    let user_info = next_account_info(account_info_iter)?;
    let authority_info = next_account_info(account_info_iter)?;
    let reserve_info = next_account_info(account_info_iter)?;

    let mut vault = TokenVault::try_from_slice(&vault_info.data.borrow())?;
    let user = User::try_from_slice(&user_info.data.borrow())?;

    assert_eq!(vault.user, *user_info.key);
    assert_eq!(reserve_info.owner, program_id);
    assert!(authority_info.is_signer);
    assert_eq!(user.authority, *authority_info.key);
    assert_eq!(vault_info.owner, program_id);

    invoke(
        &system_instruction::transfer(authority_info.key, reserve_info.key, amount * PRICE),
        &[authority_info.clone(), vault_info.clone()],
    )?;

    assert!(vault.amount + amount >= vault.amount);
    vault.amount += amount;
    vault.serialize(&mut &mut vault_info.data.borrow_mut()[..])?;

    Ok(())
}

/// Users can transfer tokens from one vault to another.
pub fn transfer(program_id: &Pubkey, accounts: &[AccountInfo], amount: u64) -> ProgramResult {
    let account_info_iter = &mut accounts.iter();

    let vault_info = next_account_info(account_info_iter)?;
    let user_info = next_account_info(account_info_iter)?;
    let authority_info = next_account_info(account_info_iter)?;
    let receiver_info = next_account_info(account_info_iter)?;

    let mut t = TokenVault::try_from_slice(&vault_info.data.borrow())?;
    let mut r = TokenVault::try_from_slice(&receiver_info.data.borrow())?;
    let u = User::try_from_slice(&user_info.data.borrow())?;

    assert_eq!(t.user, *user_info.key);
    assert!(authority_info.is_signer);
    assert_eq!(vault_info.owner, program_id);
    assert_eq!(receiver_info.owner, program_id);
    assert_eq!(u.authority, *authority_info.key);

    assert!(t.amount >= amount);
    assert!(r.amount + amount >= r.amount);
    t.amount -= amount;
    r.amount += amount;

    t.serialize(&mut &mut vault_info.data.borrow_mut()[..])?;
    r.serialize(&mut &mut receiver_info.data.borrow_mut()[..])?;

    Ok(())
}

/// This function allows a user to payout tokens from his vault to multiple other vaults.
/// Each user receives the same amount of tokens.
pub fn distribute(program_id: &Pubkey, accounts: &[AccountInfo], amount: u64) -> ProgramResult {
    let account_info_iter = &mut accounts.iter();

    let vault_info = next_account_info(account_info_iter)?;
    let user_info = next_account_info(account_info_iter)?;
    let authority_info = next_account_info(account_info_iter)?;

    let count = accounts.len() - 3;
    let receiver_infos = next_account_infos(account_info_iter, count)?;

    let mut token_vault = TokenVault::try_from_slice(&vault_info.data.borrow())?;

    assert_eq!(token_vault.user, *user_info.key);
    assert!(authority_info.is_signer);
    assert_eq!(vault_info.owner, program_id);
    let user = User::try_from_slice(&user_info.data.borrow())?;
    assert_eq!(user.authority, *authority_info.key);

    let total = amount * (count as u64);

    assert!(token_vault.amount >= total);

    token_vault.amount -= total;
    token_vault.serialize(&mut &mut vault_info.data.borrow_mut()[..])?;

    for receiver_info in receiver_infos {
        assert_eq!(receiver_info.owner, program_id);

        let mut receiver = TokenVault::try_from_slice(&receiver_info.data.borrow())?;

        receiver.amount += amount;
        receiver.serialize(&mut &mut receiver_info.data.borrow_mut()[..])?;
    }

    Ok(())
}

/// Users can sell their tokens to receive back lamports.
pub fn sell(program_id: &Pubkey, accounts: &[AccountInfo], amount: u64) -> ProgramResult {
    let account_info_iter = &mut accounts.iter();

    let vault_info = next_account_info(account_info_iter)?;
    let user_info = next_account_info(account_info_iter)?;
    let authority_info = next_account_info(account_info_iter)?;
    let reserve_info = next_account_info(account_info_iter)?;

    let mut vault = TokenVault::try_from_slice(&vault_info.data.borrow())?;
    let user = User::try_from_slice(&user_info.data.borrow())?;

    assert_eq!(vault_info.owner, program_id);
    assert_eq!(vault.user, *user_info.key);
    assert!(authority_info.is_signer);
    assert_eq!(user.authority, *authority_info.key);
    assert_eq!(reserve_info.owner, program_id);

    **authority_info.lamports.borrow_mut() += amount * PRICE;
    **reserve_info.lamports.borrow_mut() -= amount * PRICE;

    assert!(vault.amount >= amount);
    vault.amount -= amount;
    vault.serialize(&mut &mut vault_info.data.borrow_mut()[..])?;

    Ok(())
}

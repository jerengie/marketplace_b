// This file is part of a user study. Please be aware of the following information:
// The source code is not intended for use in production and should not be reproduced
// or distributed without prior written permission. The authors of this file are in
// no way liable for any damages that occur as a result of its use.

use borsh::{BorshDeserialize, BorshSerialize};
use solana_program::{
    account_info::{next_account_info, AccountInfo},
    entrypoint::ProgramResult,
    program::invoke_signed,
    pubkey::Pubkey,
    rent::Rent,
    system_instruction,
};

use crate::{entry::User, tokens::TokenVault};

/// This struct represents a tradable item.
/// Each item has a unique id, is associated to a user and is of a specific kind.
#[repr(C)]
#[derive(Clone, Copy, Debug, Default, PartialEq, BorshSerialize, BorshDeserialize)]
pub struct Item {
    pub id: u64,
    pub user: Pubkey,
    pub kind: u64,
}

/// This struct represents an offer to buy an item.
/// Everyone can offer to buy any item for any amount of tokens.
#[repr(C)]
#[derive(Clone, Copy, Debug, Default, PartialEq, BorshSerialize, BorshDeserialize)]
pub struct Offer {
    pub item: Pubkey,
    pub from: Pubkey,
    pub amount: u64,
}

/// There are 3 actions for trading
/// - the owner of an item can accept an offer
/// - anyone can become a bidder by creating (placing) an offer
/// - the bidder can delete his own offers
#[derive(Debug, BorshDeserialize, BorshSerialize)]
pub enum ItemInstruction {
    AcceptOffer,
    PlaceOffer { amount: u64 },
    DeleteOffer,
}

/// This function allows anyone to bid on an item, i.e., to place an offer.
pub fn place_offer(program_id: &Pubkey, accounts: &[AccountInfo], amount: u64) -> ProgramResult {
    let account_info_iter = &mut accounts.iter();

    let offer_info = next_account_info(account_info_iter)?;
    let item_info = next_account_info(account_info_iter)?;
    let vault_info = next_account_info(account_info_iter)?;
    let user_info = next_account_info(account_info_iter)?;
    let authority_info = next_account_info(account_info_iter)?;

    let (ok, b) = Pubkey::find_program_address(&[item_info.key.as_ref(), user_info.key.as_ref(), &amount.to_le_bytes(), b"+offer",],program_id,);
    assert_eq!(ok, *offer_info.key);
    let user = User::try_from_slice(&user_info.data.borrow())?;
    let mut vault = TokenVault::try_from_slice(&vault_info.data.borrow())?;
    assert_eq!(vault.user, *user_info.key);
    assert!(authority_info.is_signer);
    assert_eq!(user.authority, *authority_info.key);
    assert!(vault.amount >= amount);
    invoke_signed(&system_instruction::create_account(authority_info.key,offer_info.key,Rent::default().minimum_balance(74), 74,program_id),&[authority_info.clone(), vault_info.clone()], &[&[item_info.key.as_ref(), user_info.key.as_ref(),&amount.to_le_bytes(),b"+offer",&[b],]],)?;

    let mut o = Offer::try_from_slice(&offer_info.data.borrow())?;
    o.from = *user_info.key;
    o.item = *item_info.key;
    o.amount = amount;
    vault.amount -= amount;
    vault.serialize(&mut *vault_info.data.borrow_mut())?;
    o.serialize(&mut *offer_info.data.borrow_mut())?;

    Ok(())
}

/// This function lets the owner of an item accept an offer.
pub fn accept_offer(program_id: &Pubkey, accounts: &[AccountInfo]) -> ProgramResult {
    let account_info_iter = &mut accounts.iter();

    let offer_info = next_account_info(account_info_iter)?;
    let item_info = next_account_info(account_info_iter)?;
    let vault_info = next_account_info(account_info_iter)?;
    let user_info = next_account_info(account_info_iter)?;
    let authority_info = next_account_info(account_info_iter)?;

    let mut offer = Offer::try_from_slice(&offer_info.data.borrow())?;
    let (offer_key, _) = Pubkey::find_program_address(
        &[
            item_info.key.as_ref(),
            offer.from.as_ref(),
            &offer.amount.to_le_bytes(),
            b"+offer",
        ],
        program_id,
    );
    let mut item = Item::try_from_slice(&item_info.data.borrow())?;
    let user = User::try_from_slice(&user_info.data.borrow())?;
    let mut vault = TokenVault::try_from_slice(&vault_info.data.borrow())?;

    assert_eq!(offer_key, *offer_info.key);
    assert_eq!(offer.item, *item_info.key);
    assert_eq!(item.user, *user_info.key);
    assert_eq!(vault.user, *user_info.key);
    assert_eq!(user.authority, *authority_info.key);

    item.user = offer.from;
    vault.amount += offer.amount;
    offer.amount = 0;

    item.serialize(&mut *item_info.data.borrow_mut())?;
    vault.serialize(&mut *vault_info.data.borrow_mut())?;
    offer.serialize(&mut *offer_info.data.borrow_mut())?;

    Ok(())
}

/// This function deletes an offer on behalf of the bidder.
pub fn delete_offer(program_id: &Pubkey, accounts: &[AccountInfo]) -> ProgramResult {
    let account_info_iter = &mut accounts.iter();

    let offer_info = next_account_info(account_info_iter)?;
    let vault_info = next_account_info(account_info_iter)?;
    let user_info = next_account_info(account_info_iter)?;
    let authority_info = next_account_info(account_info_iter)?;

    let mut offer = Offer::try_from_slice(&offer_info.data.borrow())?;
    let (offer_key, _) = Pubkey::find_program_address(
        &[
            offer.item.as_ref(),
            user_info.key.as_ref(),
            &offer.amount.to_le_bytes(),
            b"+offer",
        ],
        program_id,
    );

    assert_eq!(offer_key, *offer_info.key);
    let user = User::try_from_slice(&user_info.data.borrow())?;
    let mut vault = TokenVault::try_from_slice(&vault_info.data.borrow())?;
    assert_eq!(offer.from, *user_info.key);
    assert_eq!(vault.user, *user_info.key);
    assert!(authority_info.is_signer);
    assert_eq!(user.authority, *authority_info.key);

    vault.amount += offer.amount;
    offer.amount = 0;
    vault.serialize(&mut *vault_info.data.borrow_mut())?;
    offer.serialize(&mut *offer_info.data.borrow_mut())?;

    let rent = offer_info.lamports();
    **authority_info.lamports.borrow_mut() += rent;
    **offer_info.lamports.borrow_mut() -= rent;

    Ok(())
}

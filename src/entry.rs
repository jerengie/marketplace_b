// This file is part of a user study. Please be aware of the following information:
// The source code is not intended for use in production and should not be reproduced
// or distributed without prior written permission. The authors of this file are in
// no way liable for any damages that occur as a result of its use.

use borsh::{BorshDeserialize, BorshSerialize};
use solana_program::{
    account_info::{next_account_info, AccountInfo},
    entrypoint,
    entrypoint::ProgramResult,
    program::{invoke_signed},
    pubkey::Pubkey,
    rent::Rent,
    system_instruction, program_error::ProgramError,
};
use crate::{tokens::TokenInstruction, item::ItemInstruction, partner::PartnerInstruction};

/// This struct represents a user associated with this program.
#[repr(C)]
#[derive(Clone, Debug, Default, PartialEq, BorshSerialize, BorshDeserialize)]
pub struct User {
    pub authority: Pubkey,
    pub name: String,
}

/// Users can be created (Register) or choose a different name (Rename).
#[derive(Debug, BorshDeserialize, BorshSerialize)]
pub enum UserInstruction {
    Register { name: String },
    Rename {name: String}
}

/// This struct is a wrapper around all possible instructions.
#[derive(Debug, BorshDeserialize, BorshSerialize)]
pub enum Instructions {
    UserScope { ui: UserInstruction },
    TokenScope { ti: TokenInstruction },
    ItemScope { ii: ItemInstruction },
    PartnerScope { pi: PartnerInstruction }
}

entrypoint!(process_instruction);

pub fn process_instruction(
    program_id: &Pubkey,
    accounts: &[AccountInfo],
    mut instruction_data: &[u8],
) -> ProgramResult {
    if let Ok(Instructions::UserScope { ui })  = Instructions::deserialize(&mut instruction_data) {
        match ui {
            UserInstruction::Register { name } => register(program_id, accounts, name),
            UserInstruction::Rename { name } => rename(program_id, accounts, name)
        }
    }
    else if let Ok(Instructions::TokenScope { ti })  = Instructions::deserialize(&mut instruction_data) {
        match ti {
            TokenInstruction::Initialize => crate::tokens::initialize(program_id, accounts),
            TokenInstruction::Buy { amount } => crate::tokens::buy(program_id, accounts, amount),
            TokenInstruction::Distribute { amount } => crate::tokens::distribute(program_id, accounts, amount),
            TokenInstruction::Sell { amount } => crate::tokens::sell(program_id, accounts, amount),
            TokenInstruction::Transfer { amount } => crate::tokens::transfer(program_id, accounts, amount),
        }
    } else if let Ok(Instructions::PartnerScope { pi }) = Instructions::deserialize(&mut instruction_data) {
        match pi {
            PartnerInstruction::ApplyCoupon { seed } => crate::partner::apply_coupon(program_id, accounts, seed),
            PartnerInstruction::RefreshCredit { seed } => crate::partner::apply_coupon(program_id, accounts, seed)
        }
    }
    else
    if let Ok(Instructions::ItemScope { ii }) = Instructions::deserialize(&mut instruction_data) {
        match ii {
            ItemInstruction::AcceptOffer => crate::item::accept_offer(program_id, accounts),
            ItemInstruction::PlaceOffer { amount } => crate::item::place_offer(program_id, accounts, amount),
            ItemInstruction::DeleteOffer => crate::item::delete_offer(program_id, accounts)
        }
    }
    else {
        Err(ProgramError::InvalidInstructionData)
    }
}

/// This function creates a PDA to hold user data for the authority account.
pub fn register(program_id: &Pubkey, accounts: &[AccountInfo], name: String) -> ProgramResult {
    let account_info_iter = &mut accounts.iter();

    let user_info = next_account_info(account_info_iter)?;
    let authority_info = next_account_info(account_info_iter)?;

    let (uk, ubump) =
        Pubkey::find_program_address(&[authority_info.key.as_ref(), b"+user"], program_id);

    assert!(authority_info.is_signer);
    assert_eq!(*user_info.key, uk);

    invoke_signed(
        &system_instruction::create_account(
            authority_info.key,
            user_info.key,
            Rent::default().minimum_balance(1024),
            42,
            program_id,
        ),
        &[authority_info.clone(), user_info.clone()],
        &[&[authority_info.key.as_ref(), b"+user", &[ubump]]],
    )?;

    let mut u = User::try_from_slice(&user_info.data.borrow())?;
    u.authority = *authority_info.key;
    u.name = name;
    u.serialize(&mut *user_info.data.borrow_mut())?;

    Ok(())
}

/// This function allows an authority account to rename the user associated to it.
pub fn rename(program_id: &Pubkey, accounts: &[AccountInfo], name: String) -> ProgramResult {
    let account_info_iter = &mut accounts.iter();

    let user_info = next_account_info(account_info_iter)?;
    let authority_info = next_account_info(account_info_iter)?;

    let mut u = User::try_from_slice(&user_info.data.borrow())?;
    u.name = name;
    assert!(authority_info.is_signer);
    assert_eq!(user_info.owner, program_id);
    assert_eq!(u.authority, *authority_info.key);
    assert_eq!(authority_info.owner, program_id);

    Ok(())
}